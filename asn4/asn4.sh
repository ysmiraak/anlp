#!/usr/bin/env bash

java -mx12g -cp "stanford-parser-full-2017-06-09/*:" \
     edu.stanford.nlp.parser.lexparser.LexicalizedParser \
     -nthreads -1 \
     -sentences newline \
     -tokenized \
     -outputFormat "oneline" \
     -writeOutputFiles \
     edu/stanford/nlp/models/lexparser/wsjPCFG.ser.gz \
     parsing-eval/wsj.22.mrg.text

cd parsing-eval
mv wsj.22.mrg.text.stp wsj.22.mrg.stanford.wsjPCFG
