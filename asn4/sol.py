#############
# parse_ptb #
#############

import re


def parse_ptb(line, line_id= 0
              , normalize= None
              , label= re.compile(r"\w*")
              , unpack= re.compile(r"ROOT|TOP")
              , delete= re.compile(r"-NONE-")):
    """returns a ptb parse tree from `line` which may be a multi-line
    string but must contain only one tree (top level bracket).

    non-terminal symbols will be indexed by their positions in the
    sentence, and prepended by `line_id`, after being normalized by
    function `normalize: str -> str`.

    terminal symbols are retrieved by taking the first result produced
    by `label.search`.  when fullmatched by `delete`, the entire node
    is removed.  when fullmatched by `unpack`, only the terminal
    symbol is removed, the children nodes are unpacked into the
    higher-level node.

    """
    if not normalize: normalize = lambda x: x
    stack, symbol, term_id = [[]], [], 0
    for char in line:
        if char.isspace():
            if symbol: # symbol is a non-terminal
                stack[-1].append("".join(symbol))
                symbol = []
        elif ")" == char:
            node = stack.pop()
            head = node[0]
            if isinstance(head, tuple): # dummy node
                assert not symbol
                stack[-1].extend(node)
            elif re.fullmatch(delete, head):
                symbol = []
                pass
            else:
                if symbol: # symbol is a terminal
                    node.append("{}.{} {}".format(line_id, term_id, normalize("".join(symbol))))
                    symbol = []
                    term_id += 1
                if 1 == len(node): # node is empty due to delete
                    pass
                elif re.fullmatch(unpack, head):
                    stack[-1].extend(node[1:])
                else:
                    node[0] = re.search(label, head).group()
                    stack[-1].append(tuple(node))
        elif "(" == char:
            assert not symbol
            stack.append([])
        else:
            symbol.append(char)
    res = stack.pop()
    assert (not symbol) and (not stack) and (1 == len(res))
    return res[-1]


def load_ptb(file, skip_err= True, verbose= True, **kwargs):
    """yields ptb trees in `file`.

    if `skip_err`, yields an empty tree when a line cannot be parsed.
    otherwise raise the error.

    if `verbose`, prints info for parsing errors.

    any additional keyword arguments will be given to `parse_ptb`.

    """
    with open(file, encoding= 'utf-8') as file:
        for i, line in enumerate(file):
            try:
                tree = parse_ptb(line, line_id= i, **kwargs)
            except Exception as err:
                if verbose:
                    print("failed to parse line {}: {}".format(i, line), end= '')
                if skip_err:
                    yield ()
                else:
                    raise err
            else:
                yield tree

##################
# test parse_ptb #
##################

s = "( (S (ROOT (VP-STUFF (VBD Reported) (SBAR (-NONE- 0) (S (-NONE- *T*-1)))) (. .))))"
print("""
* test parse_ptb on

- delete -NONE-
- unwrap ROOT
- unwrap dummy root
- normalize symbols

** line

{}

** tree

{}""".format(s, parse_ptb(s)))

################
# constituents #
################

from collections import namedtuple, Counter, defaultdict


Cons = namedtuple('Cons', 'span label count')
Cons.default = Cons(span= (), label= '', count= 0)


def constituents(tree, labeled= True, counted= True, orphan= '????'):
    """yields constituents in `tree`.  each constituent is a namedtuple of
    terminal symbols `span: (tuple str)`, the non-terminal symbol
    `label: str`, and an additional identifier `count: int`.

    postag nodes do not count as constituents.  so `tree` should not
    be a preterminal node such as `(VBD Telecussed)`.  nevertheless,
    should that happen, the tree is wrapped in an `orphan` node like
    `(???? (VBD Telecussed))`.

    """
    assert isinstance(tree, tuple)
    if not tree: return
    # tree should not be a terminal derivation, nevertheless
    if isinstance(tree[-1], str): tree = orphan, tree
    if counted: counter = Counter()
    span = []
    for sub in tree[1:]:
        if isinstance(sub[-1], str): # sub is a terminal derivation
            span.append(sub[-1])
        else: # sub is a non-terminal derivation
            for cons in constituents(sub, labeled= labeled, counted= False):
                if counted:
                    yield cons._replace(count= counter[cons])
                    counter[cons] += 1
                else:
                    yield cons
            span.extend(cons.span)
    cons = Cons(count= 0, span= tuple(span), label= tree[0] if labeled else '')
    if counted: cons = cons._replace(count= counter[cons])
    yield cons


def span(tree):
    """returns the span of non-terminals (tuple str) covered by `tree`."""
    cons = Cons.default
    for cons in constituents(tree, labeled= False, counted= False): pass
    return cons.span


def match(gold, test, verbose= True):
    """yields ptb trees in `test`.  trees in `gold` and `test` are
    compared in sequence.  if their non-terminals match up, yields the
    test tree, otherwise yields an empty tree.

    """
    assert len(gold) == len(test)
    for i, (g, t) in enumerate(zip(gold, test)):
        if span(g) == span(t):
            yield t
        else:
            if verbose:
                print("sent {} mismatch in gold and test".format(i))
            yield ()

#####################
# test constituents #
#####################

s = "(S (A (P this)) (B (Q is) (A (A (C (A (A (R a) (T test))))))))"
p = parse_ptb(s)
print("""
* test constituents on

- labeling
- counting

** tree

{}""".format(p))
print("\n** labeled\n")
for c in constituents(p): print(c)
print("\n** unlabeled\n")
for c in constituents(p, labeled= False): print(c)

###########
# metrics #
###########

def partition(it, f):
    """-> defaultdict b (set a), where it: iterable a, f: a -> b

    returns the partition of elements in `it` by the equivalence
    kernel of `f`.

    """
    res = defaultdict(set)
    for x in it: res[f(x)].add(x)
    return res


def precision(gold, test):
    """returns the precision of `test` set on `gold` set."""
    assert isinstance(gold, set)
    assert isinstance(test, set)
    try:
        return sum(x in gold for x in test) / len(test)
    except ZeroDivisionError:
        return 0.0


def recall(gold, test):
    """returns the recall of `test` set on `gold` set."""
    return precision(test, gold)


def f1(p, r):
    """returns the f1 score given precision `p` and recall `r`."""
    try:
        return 2 * p * r / (p + r)
    except ZeroDivisionError:
        return 0.0


import pandas as pd
pd.options.display.width = 250
pd.options.display.float_format = "{:.4f}".format


def metrics(gold, test, by= None):
    """returns a pandas DataFrame with `f1`, `prec`, and `recall` scores.
    `gold` and `test` need not be sets, but they will be partitioned
    by the equivalence kernel of `by`, so duplicates will not be
    counted.  each equivalence class gets a row.

    """
    if by is None: by = lambda _: ''
    gold = partition(gold, by)
    test = partition(test, by)
    rows = tuple({*gold, *test})
    p = [precision(gold[row], test[row]) for row in rows]
    r = [recall(gold[row], test[row]) for row in rows]
    f = [f1(p, r) for p, r in zip(p, r)]
    return pd.DataFrame(data= {'f1': f, 'prec': p, 'recall': r}, index= rows)

#############################
# test metrics agains evalb #
#############################

print("\n* test load_ptb and metrics agains evalb\n")
gold = tuple(load_ptb("EVALB/sample/sample.gld", normalize= str.lower))
test = tuple(load_ptb("EVALB/sample/sample.tst", normalize= str.lower))
test = tuple(match(gold, test))

print("\n** scores per sentence\n")
print(pd.concat(
    (metrics(constituents(g), constituents(t)) for g, t in zip(gold, test))
    , ignore_index= True))

print("\n** scores macro-averaged\n")
print(metrics(
    gold= {c for t in gold for c in constituents(t)}
    , test= {c for t in test for c in constituents(t)}))

#############################
# Problem 2: Error Analysis #
#############################

print("\n* evaluate the berkeley parser (berk) and the stanford parser (stan)\n")
gold = tuple(load_ptb("parsing-eval/wsj.22.mrg.gold"))
berk = tuple(load_ptb("parsing-eval/wsj.22.mrg.berkeley"))
stan = tuple(load_ptb("parsing-eval/wsj.22.mrg.stanford.wsjPCFG"))
berk = tuple(match(gold, berk))
stan = tuple(match(gold, stan))

print("\n** unlabeled\n")
cons_gold = {c for t in gold for c in constituents(t, labeled= False)}
cons_berk = {c for t in berk for c in constituents(t, labeled= False)}
cons_stan = {c for t in stan for c in constituents(t, labeled= False)}
metrics_berk = metrics(gold= cons_gold, test= cons_berk)
metrics_stan = metrics(gold= cons_gold, test= cons_stan)
metrics_berk.index = 'berk',
metrics_stan.index = 'stan',
print(pd.concat((metrics_berk, metrics_stan)))

print("\n** labeled\n")
cons_gold = {c for t in gold for c in constituents(t)}
cons_berk = {c for t in berk for c in constituents(t)}
cons_stan = {c for t in stan for c in constituents(t)}
metrics_berk = metrics(gold= cons_gold, test= cons_berk)
metrics_stan = metrics(gold= cons_gold, test= cons_stan)
metrics_berk.index = 'berk',
metrics_stan.index = 'stan',
print(pd.concat((metrics_berk, metrics_stan)))

###########
# by size #
###########

by_size = lambda cons: len(cons.span)
metrics_berk = metrics(gold= cons_gold, test= cons_berk, by= by_size)
metrics_stan = metrics(gold= cons_gold, test= cons_stan, by= by_size)
metrics_berk.columns = map("{}:berk".format, metrics_berk.columns)
metrics_stan.columns = map("{}:stan".format, metrics_stan.columns)
m = pd.concat((metrics_berk, metrics_stan), axis= 1, join= 'inner')
m.sort_index(axis= 0, inplace= True)
import matplotlib.pyplot as plt
for metric in 'f1', 'prec', 'recall':
    m[["{}:berk".format(metric), "{}:stan".format(metric)]].plot()
    plt.title("{} by constituent size".format(metric))
    plt.xlabel("constituent size")
    plt.ylabel(metric)
    plt.show()

############
# by label #
############

by_label = lambda cons: cons.label
metrics_berk = metrics(gold= cons_gold, test= cons_berk, by= by_label)
metrics_stan = metrics(gold= cons_gold, test= cons_stan, by= by_label)
metrics_berk.columns = map(lambda s: s[0] + ":berk", metrics_berk.columns)
metrics_stan.columns = map(lambda s: s[0] + ":stan", metrics_stan.columns)
labels = sorted({*metrics_berk.index, *metrics_stan.index})
cons_bl_gold = partition(cons_gold, by_label)
cons_bl_berk = partition(cons_berk, by_label)
cons_bl_stan = partition(cons_stan, by_label)
m = pd.concat(
    (metrics_berk, metrics_stan, pd.DataFrame(
        data= {'#': [len(cons_bl_gold[label]) for label in labels]
               , '#berk': [len(cons_bl_berk[label]) for label in labels]
               , '#stan': [len(cons_bl_stan[label]) for label in labels]}
        , index= labels))
    , axis= 1)
m.sort_values(by= '#', inplace= True)
m.sort_index(axis= 1, inplace= True)
print(m)
