###############################
# Problem 1: Word Recognition #
###############################


from collections import defaultdict
from itertools import product
import re


def load_cnf(file, re_line= re.compile(r"\s*(\w+)\s*->\s*(?:(\w+)\s+(\w+)|\"(.+)\")\s*")):
    """->
    defaultdict      : grammar
        either       : rhs
            str      : unary (terminal) rules
            str, str : binary (non-terminal) rules
        set str      : lhs
    """
    cnf = defaultdict(set)
    with open(file, encoding= 'utf-8') as file:
        for line in file:
            lhs, rhs1, rhs2, term = re_line.fullmatch(line).groups()
            cnf[term if term else (rhs1, rhs2)].add(lhs)
    return cnf


def cky(cnf, sent):
    """->
    defaultdict  : chart
        int, int : span
        set str  : lhs
    """
    chart = defaultdict(set)
    for j, term in enumerate(sent, 1):
        chart[(j-1, j)].update(cnf[term])
        for i in range(j-1, -1, -1):
            cell = i, j
            for k in range(i+1, j):
                for rhs in product(chart[(i, k)], chart[(k, j)]):
                    chart[(i, j)].update(cnf[rhs])
    return chart


def last_cell(chart):
    """returns the span of the last cell in `chart`."""
    return 0, int(((len(chart) * 8 + 1) ** 0.5 - 1) / 2)


def recognize(cnf, sent, root= 'SIGMA'):
    """returns whether `sent` is recognized by `cnf` as `root`."""
    chart = cky(cnf, sent)
    return root in chart[last_cell(chart)]


######################
# Problem 2: Parsing #
######################


def cky2(cnf, sent):
    """->
    defaultdict                       : chart
        int, int                      : span
        defaultdict                   : cell
            str                       : lhs
            either                    : rhs
                str                   : terminal
                set ((str, str), int) : non-terminal, backpointer
    """
    chart = defaultdict(lambda: defaultdict(set))
    for j, term in enumerate(sent, 1):
        for nont in cnf[term]:
            chart[(j-1, j)][nont] = term
        for i in range(j-1, -1, -1):
            cell = i, j
            for k in range(i+1, j):
                for rhs in product(chart[(i, k)], chart[(k, j)]):
                    back = rhs, k
                    for lhs in cnf[rhs]:
                        chart[cell][lhs].add(back)
    return chart


def trees(chart, i, j, root):
    """yields trees with `root` which span over `i, j` in `chart`."""
    if 1 == j - i:
        yield root, chart[(i, j)][root]
    else:
        for (rhs1, rhs2), k in chart[(i, j)][root]:
            for tree1, tree2 in product(
                    trees(chart, i, k, rhs1)
                    , trees(chart, k, j, rhs2)):
                yield root, tree1, tree2


def parse(cnf, sent, root= 'SIGMA'):
    """yields parse trees of `sent` with `root` according to `cnf`."""
    chart = cky2(cnf, sent)
    return trees(chart, *last_cell(chart), root)


############
# examples #
############


cnf = load_cnf("atis-grammar-cnf.cfg")

import nltk

sents = [sent for sent, _ in nltk.parse.util.extract_test_sentences(
    nltk.data.load("grammars/large_grammars/atis_sentences.txt", "auto"))]

grammatical = []
ungrammatical = []
for sent in sents:
    if recognize(cnf, sent):
        grammatical.append(sent)
    else:
        ungrammatical.append(sent)
grammatical.sort(key= len)
ungrammatical.sort(key= len)

print("\n------------------- grammatical -------------------")
for sent in grammatical[:10]: print(*sent)
print("\n------------------ ungrammatical ------------------")
for i, sent in enumerate(ungrammatical[:10]): print("{}.".format(i), *sent)



print("\t".join((str(sum(1 for _ in parse(cnf, sent))) for sent in sents)))


from pprint import pprint
from io import StringIO

clean = str.maketrans({ord(","): None, ord("'"): None})

sent = grammatical[9]
print(*sent)
for i, tree in enumerate(parse(cnf, sent), 1):
    print("\n{}. parse\n".format(i))
    with StringIO() as s:
        pprint(tree, stream= s)
        print(s.getvalue().translate(clean))
