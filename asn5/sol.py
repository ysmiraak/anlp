###############
# IBM Model 1 #
###############

# Improve over the baseline by implementing an aligner based on IBM
# Model 1 (see “The Challenge” section of the JHU course for a more
# detailed description). IBM Model 1 is a probabilistic model that
# generates each word of the English sentence independently,
# conditioned on some word in the Foreign sentence. Thus, its key
# features are the word translation parameters P(e|f)P(e|f), which are
# in turn learned from the data using expectation maximization
# (EM). These are the two focus points for your implementation. Feel
# free to use NLTK if you find it helpful, but note that anything in
# the nltk.align package is disallowed in this assignment.

# Evaluate your Model 1 aligner and discuss the results. What
# influence does the size of the training data have?

"en600.468/aligner/data/hansards.a"
"en600.468/aligner/data/hansards.e"
"en600.468/aligner/data/hansards.f"

#########################
# Improving the Aligner #
#########################

# Implement any of the suggestions for improving over your IBM Model 1
# as suggested in the task description from the JHU course, i.e.,
# implement an aligner that improves over your IBM Model 1 AER.

# Work individually for this assignment, and please ask me before
# using any additional data sources. Use your final alignment model to
# align the hansard dataset. Provide this alignment in a separate
# file. See below ("Leaderboard") for description on how to submit the
# file.

# Briefly describe and justify your chosen model. Evaluate your model
# using AER. The code from Problem 1 includes a scoring
# function. Compare your results to the Model 1 results (and the
# baseline). Please include one example visualization of an aligned
# sentence in your model (for example, a tabbed table with 'X' for
# aligned words might work). (It's even better if you show a sentence
# that Model 1 or the baseline gets wrong - and show the wrong
# alignments as well.)

# For evaluation of your aligners, we will introduce a little healthy
# competition. We have set up a leaderboard website where you can
# upload your complete alignment files. The files are then scored
# automatically and the website displays a current ranking of all
# submissions, the leaderboard. The leaderboard will show you your
# performance compared to the other students in the class. You can
# resubmit your alignment files as many times as you want, up to the
# assignment submission deadline.

# You need a Google login to submit your results. If you don't have
# one yet, please create one (you can create a throwaway email address
# for this purpose).

# Go to the task website (https://anlp17-ass5.appspot.com/) to upload
# your results file (see Note below). As your handle, please choose
# your last name. You can choose to keep your handle hidden (it will
# still be displayed to you and the instructor), but in the spirit of
# competition you may also leave it visible to your fellow
# students. You can upload newer results at any time (only the newest
# version will appear in the ranking). If you want to submit several
# alternative systems (e.g., if you're trying out different
# improvements), you may have to use different Google logins (I'm not
# sure how else to do this).

# The submissions are scored automatically on the server and the
# results are immediately displayed here:
# https://anlp17-ass5.appspot.com/leaderboard.html The evaluation is
# done using the Alignment Error Rate (AER) on a development data
# set. I will rescore your submissions on a larger test data set after
# the submission deadline to reveal the true winner(s)!

# Note: The web app will reject files larger than 1 MB, so please
# reduce your file to only the first 1,000 lines before uploading:

# python align | head -n1000 > output.txt
