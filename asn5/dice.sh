python3 align_dice.py       -d en600.468/aligner/data/hansards -n 1000  -o baseline_1000.a
python3 align_dice.py       -d en600.468/aligner/data/hansards -n 10000 -o baseline_10000.a
python3 score-alignments.py -d en600.468/aligner/data/hansards -n 0 baseline_1000.a
python3 score-alignments.py -d en600.468/aligner/data/hansards -n 0 baseline_10000.a
