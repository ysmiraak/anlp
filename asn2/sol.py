################################
# Problem 1: Viterbi Algorithm #
################################
EISNER_STATES = ['C','H']
EISNER_INITIAL_PROBS = {'C': 0.2, 'H': 0.8}
EISNER_TRANSITIONS = {'C': {'C':0.6, 'H': 0.4}, 'H': {'C':0.3, 'H':0.7}}
EISNER_EMISSIONS = {'C': {'1':0.5,'2':0.4,'3':0.1},'H': {'1':0.2, '2':0.4,'3':0.4}}


import numpy as np


class Viterbi:
    """
    initial    : dict hidden-states initial-log-probability
    transition : dict source-states (dict target-states transition-log-probability)
    emission   : dict hidden-states (dict observation emission-log-probability)
    """
    __slots__ = '_s', '_i', '_t', '_e'

    def __init__(self, initial, transition, emission):
        # hidden states, with the same ordering as in the log probability arrays
        self._s = tuple(initial)
        # initial vector
        self._i = np.array([initial[s] for s in self._s])
        # transition matrix (target, source)
        self._t = np.array([[transition[s][t] for s in self._s] for t in self._s])
        # emission map (observation -> emission vector)
        neg_inf = float('-inf')
        self._e = {o: np.array([emission[s][o] if o in emission[s] else neg_inf for s in self._s])
                   for o in {o for e in emission.values() for o in e}}

    def decode(self, observations):
        """-> list hidden-state"""
        p = self._i.copy()
        try:
            p += self._e[observations[0]]
        except KeyError: # observation unknown
            # interpolate based on the transition probabilities
            pass
        # unfold trellis
        trellis, idxs = [], np.arange(len(p))
        for obs in observations[1:]:
            p = self._t + p
            trellis.append(np.argmax(p, axis= 1))
            p = p[idxs, trellis[-1]]
            try:
                p += self._e[obs]
            except KeyError: # observation unknown
                # interpolate based on the transition probabilities
                pass
        # fold trellis to retrieve the transition sequence
        winner = np.argmax(p)
        states = [self._s[winner]]
        # print(p[winner])
        while trellis:
            winner = trellis.pop()[winner]
            states.append(self._s[winner])
        return states[::-1]


def map_vals(d, f, *args, **kwargs):
    """-> dict; with `f` applied to all values in `d`."""
    return {k: f(v, *args, **kwargs) for k, v in d.items()}


v = Viterbi(map_vals(EISNER_INITIAL_PROBS, np.log)
            , map_vals(EISNER_TRANSITIONS, map_vals, np.log)
            , map_vals(EISNER_EMISSIONS, map_vals, np.log))
print("hidden states for observations [3 1 3]:", *v.decode("313"))


###########################
# Problem 2: HMM Training #
###########################
CORPUS_train = "wsj_tagged/wsj_tagged_train.tt"
CORPUS_eval = "wsj_tagged/wsj_tagged_eval.tt"
CORPUS_test = "wsj_tagged/wsj_tagged_test.t"


from collections import Counter, defaultdict


class AdditiveNgram:
    """with the additive smoothing parameter `alpha`."""
    __slots__ = '_n', '_sym_begin', '_sym_end', '_vocab', '_ngram2freq', '_margin2freq', 'alpha'

    def __init__(self, n, alpha= 1, sym_begin= "<s>", sym_end= "</s>"):
        assert 1 <= n
        self._n = n
        self._sym_begin = sym_begin
        self._sym_end = sym_end
        self._vocab = {sym_end} # all symbols excluding `sym_begin`
        self._ngram2freq = Counter() # ngram frequencies
        self._margin2freq = Counter() # marginal frequencies
        self.alpha = alpha

    def ngrams(self, seq):
        """return ngrams from `seq` with paddings."""
        padded_seq = [self._sym_begin for _ in range(self._n - 1)]
        padded_seq.extend(seq)
        padded_seq.append(self._sym_end)
        return zip(*[padded_seq[i:] for i in range(self._n)])

    def update(self, seq):
        """update this model with `seq`."""
        if not isinstance(seq, list): seq = list(seq)
        self._vocab.update(seq)
        for ngram in self.ngrams(seq):
            self._ngram2freq[ngram] += 1
            self._margin2freq[ngram[:-1]] += 1

    def train(self, data):
        """train this model with `data`."""
        for seq in data: self.update(seq)

    def log_cond_prob(self, ngram):
        """return the log probability of `ngram[-1]` given `ngram[:-1]`."""
        freq = self._ngram2freq[ngram] + self.alpha
        return (np.log(freq)
                - np.log(self._margin2freq[ngram[:-1]]
                         + (self.alpha * len(self._vocab)))) if freq else float('-inf')

    def log_prob(self, seq):
        """return the log probability of `seq`."""
        return np.fromiter(map(self.log_cond_prob, self.ngrams(seq)), dtype=np.float).sum()

    def log_likelihood(self, data):
        """return the log likelihood of `data`."""
        return np.fromiter(map(self.log_prob, data), dtype=np.float).sum()


def load_corpus(file, sep= None, encoding= 'utf-8'):
    """-> iter (list (str | (list str)))"""
    with open(file, mode= 'r', encoding= encoding) as file:
        sent = []
        for line in file:
            line = line.strip()
            if line:
                sent.append(line.split(sep) if sep else line)
            elif sent:
                yield sent
                sent = []
        if sent: yield sent


data = []
emission = defaultdict(Counter)
# load training data and emission frequencies
for sent in load_corpus(CORPUS_train, sep= "\t"):
    tags = []
    for tok, tag in sent:
        tags.append(tag)
        emission[tag][tok] += 1
    data.append(tags)
del sent, tags, tok, tag
# convert emission frequencies to log probabilities
emission = map_vals(
    emission
    , lambda counter: map_vals(
        counter
        , lambda freq, tot_count: np.log(freq / tot_count)
        , float(sum(counter.values()))))
# train the bigram model
model = AdditiveNgram(2)
model.train(data)


def make_tagger(alpha):
    model.alpha = alpha
    return Viterbi(
        emission= emission
        , initial= {t: model.log_cond_prob((model._sym_begin, t)) for t in emission}
        , transition= {s: {t: model.log_cond_prob((s, t)) for t in emission} for s in emission})


tagger_unsmoothed = make_tagger(0)
tagger_laplace = make_tagger(1)


def cross_validation(data, train_test, fold= 10):
    """performs n-fold cross-validation.

    `data` is split by `fold` in two parts: `train` & `test`;

    yields `train_test(train, test)` for each fold.

    """
    assert isinstance(data, list)
    assert 1 <= fold
    split = round(len(data) / fold)
    for n in range(fold):
        i = split * n
        j = split * (n + 1)
        yield train_test(data[:i] + data[j:], data[i:j])


def train_test(train, test):
    """find the smoothing parameter by a grid search in range [0, 1] with
    precision 0.1 which maximizes the log likelihood.

    """
    model = AdditiveNgram(2)
    model.train(train)
    alpha, likelihood = 0, float('-inf')
    for alpha2 in np.linspace(start= 0, stop= 1, num= 11):
        model.alpha = alpha2
        likelihood2 = model.log_likelihood(test)
        if likelihood < likelihood2:
            alpha, likelihood = alpha2, likelihood2
    return alpha


# 3-fold cross-validation for finding the optimal smoothing parameter,
alpha = np.fromiter(cross_validation(data, train_test, fold= 3), dtype=float)
print("optimal smoothing parameters in 3-fold cross-validation:", *alpha)
alpha.sort()
alpha = alpha[1] # take the middle one
print("the smoothing parameter chosen:", alpha)
tagger_lidstone = make_tagger(alpha)


#########################
# Problem 3: Evaluation #
#########################

from tagging_eval import eval_tags


def save_corpus(file, corpus, encoding= 'utf-8'):
    """corpus : seq (list (tok: str, tag: str))"""
    with open(file, mode= 'w', encoding= encoding) as file:
        for sent in corpus:
            print(*map("\t".join, sent), sep= "\n", end= "\n\n", file= file)


fnames = "tagged_unsmoothed.tt", "tagged_laplace.tt", "tagged_lidstone.tt"
taggers = tagger_unsmoothed,      tagger_laplace,      tagger_lidstone

test = list(load_corpus(CORPUS_test, sep= None))

for fname, tagger in zip(fnames, taggers):
    save_corpus(fname, (zip(sent, tagger.decode(sent)) for sent in test))
    print("\n------------------", fname, "------------------")
    eval_tags(CORPUS_eval, fname)
del fname, tagger
