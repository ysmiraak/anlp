def eval_tags(fname_gold, fname_model):
    n = 0
    pr_counts = {}
    with open(fname_gold) as gold, open(fname_model) as model:
        for line_gold, line_model in zip(gold, model):
            line_gold = line_gold.rstrip()
            line_model = line_model.rstrip()
            n += 1
            if len(line_gold) == 0:
                continue

            word_gold, tag_gold = line_gold.split("\t")
            word_model, tag_model = line_model.split("\t")

            # word forms should match in gold and system file
            if word_model != word_gold:
                raise ValueError("\nError in line {}: Words in gold and model files are different. Either your gold "
                                 "file is broken or your model file was generated incorrectly.".format(n))

            if tag_gold not in pr_counts:
                pr_counts[tag_gold] = {"correct": 0, "total_model": 0, "total_gold": 0}
            if tag_model not in pr_counts:
                pr_counts[tag_model] = {"correct": 0, "total_model": 0, "total_gold": 0}
            pr_counts[tag_gold]["total_gold"] += 1
            pr_counts[tag_model]["total_model"] += 1
            if tag_gold == tag_model:
                pr_counts[tag_model]["correct"] += 1

        print("\nComparing gold file {} with model output file {}...".format(fname_gold, fname_model))
        correct = 0
        total = 0
        print("\nTAG / Precision / Recall / F1\n")
        for tag, counts in pr_counts.items():
            correct += counts["correct"]
            total += counts["total_gold"]
            if 0 in counts.values():
                print("Tag {} was never correctly assigned. One of precision, recall or F1 is undefined!".format(tag))
            else:
                precision = counts["correct"] / counts["total_model"]
                recall = counts["correct"] / counts["total_gold"]
                f1 = (2 * precision * recall) / (precision + recall)
                print("{:5s}: {:4f} / {:4f} / {:4f}".format(tag, precision, recall, f1))

        print("\nOverall Accuracy: {:5f}".format(correct / total))
