#############
# problem 1 #
#############

KJBIBLE = "kjbible.txt"  # do not change file names
JBOOK = "junglebook.txt"
SETIMES_TR = "SETIMES2.bg-tr.tr"
SETIMES_BG = "SETIMES2.bg-tr.bg"

from collections import Counter
import matplotlib.pyplot as plt
import nltk

# because `nltk.word_tokenize` has no bulgarian model, i'm just using
# a generic tokenizer here.
def tokenize(file, tokenizer= nltk.tokenize.WordPunctTokenizer()):
    "-> list str"
    with open(file, encoding='utf-8') as file:
        return tokenizer.tokenize(file.read())


names = 'KJBIBLE', 'JBOOK', 'SETIMES_TR', 'SETIMES_BG'
files = tuple(map(eval, names))
texts = tuple(map(tokenize, files))


for idx, name in enumerate(names):
    freq = [f for _, f in Counter(texts[idx]).most_common()]
    plt.subplot(len(names), 2, idx * 2 + 1)
    plt.plot(freq)
    plt.xlabel("linear")
    plt.ylabel(name)
    plt.subplot(len(names), 2, idx * 2 + 2)
    plt.loglog(freq)
    plt.xlabel("loglog")
    del freq
plt.show()

#############
# problem 2 #
#############

# i'm using the king james bible here, generating 12 examples of
# length 100 for each ngram model with n in [2, 4].

from collections import deque
from itertools import repeat, islice
from ngram import NgramModel

text = texts[0]
model = NgramModel(4, text, nltk.probability.LaplaceProbDist)

def generate(model):
    "-> stream str"
    n = model._n - 1
    context = deque(repeat("", n), n)
    while True:
        t = model.generate_one(context)
        yield t
        context.append(t)


num_eg = 12
len_eg = 100
print("\n\n* n = 4\n")
for _ in range(num_eg): print(*islice(generate(model), len_eg))
print("\n\n* n = 3\n")
for _ in range(num_eg): print(*islice(generate(model.backoff), len_eg))
print("\n\n* n = 2\n")
for _ in range(num_eg): print(*islice(generate(model.backoff.backoff), len_eg))

#############
# problem 3 #
#############

text = texts[0]
word2freq = Counter(text)
pair2freq = Counter(zip(text, text[1:]))

pair2pmi = {}
for pair, freq in pair2freq.items():
    freq1, freq2 = word2freq[pair[0]], word2freq[pair[1]]
    if (10 <= freq1) and (10 <= freq2):
        pair2pmi[pair] = (freq * len(text)) / (freq1 * freq2)

pair_pmi = sorted(pair2pmi.items(), key= lambda pp: pp[1])

print("\n\n* 20 highest pmi\n")
for (w1, w2), pmi in pair_pmi[-20:]: print(w1, w2, pmi, sep="\t")
print("\n\n* 20 lowest pmi\n")
for (w1, w2), pmi in pair_pmi[:20]: print(w1, w2, pmi, sep="\t")
